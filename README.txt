-- SUMMARY --

The JW Player module adds a new field for displaying video files in a JW Player.

For a full description visit the project page:
  http://drupal.org/project/jw_player
Bug reports, feature suggestions and latest developments:
  http://drupal.org/project/issues/jw_player


-- REQUIREMENTS --

* This module depends on the File module, which is part of Drupal core as well
  as the Libraries module (http://drupal.org/project/libraries).


-- INSTALLATION --

* Download either the latest commercial or the latest non-commercial JW
  Player at http://www.longtailvideo.com/players/jw-flv-player/.

* Extract the zip file and put the contents of the extracted folder in
  libraries/jwplayer. 
  E.g.: sites/all/libraries/jwplayer or sites/<sitename>/libraries/jwplayer
	
* Install this module as described at http://drupal.org/node/895232.

* Go to Administration > Reports > Status reports (admin/reports/status) to
  check your configuration.